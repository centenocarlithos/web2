<?php
  class Instructor extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertar($datos){
        return $this->db
                ->insert("instructor",
                $datos);
    }


    //FUNCION PARA CONSULTAR INSTRUCTORES
    function obtenerTodos(){
      $listadoInstructores=
      $this->db->get("instructor");
      //VALIDACION
      if($listadoInstructores
        ->num_rows()>0){//SI hay datos
          //varios registros con result
        return $listadoInstructores->result();
      }else{//No hay datos
        return false;
      }
    }

    //FUNCION PARA BORRAR INSTRUCTORES
    // function borrar($id_ins){
    //   $this->db->where("id_ins",$id_ins);
    //   //VALIDACION
    //   if ($this->db->delete("instructor")) {
    //     return true;
    //   } else {
    //     return false;
    //   }
    // }

    function borrar($id_ins){
      $this->db->where("id_ins",$id_ins);
      return $this->db->delete("instructor");
    }

    //FUNCION PARA CONSULTAR UN INSTRUCTOR EN ESPECIFICO
    function obtenerPorId($id_ins){
      $this->db->where("id_ins",$id_ins);
      $instructor=$this->db->get("instructor");
      if ($instructor->num_rows()>0){
        //un solo instructor con row
        return $instructor->row();
      }
      return false;
    }

    //FUNCION PARA ACTUALIZAR UN INSTRUCTOR , recibe id dle instructor nuevos datos y se envia a actualizar
    function actualizar($id_ins,$datos){
      $this->db->where("id_ins",$id_ins);
      return $this->db->update('instructor',$datos);
    }

  }//Cierre de la clase

 ?>

<?php
  class Estudiante extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertar($datos){
        return $this->db
                ->insert("estudiante",
                $datos);
    }

    function obtenerTodo(){
      $listadoEstudiantes=
      $this->db->get("estudiante");
      //VALIDACION
      if ($listadoEstudiantes
        ->num_rows()>0){
          return $listadoEstudiantes->result();
      } else {
        return false;
      }
    }


    function borrar($id_es){
      $this->db->where("id_es",$id_es);
      return $this->db->delete("estudiante");
    }
  }//Cierre de la clase

 ?>

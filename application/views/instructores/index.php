<div class="row">
  <div class="col-md-8">
    <h1>LISTADO DE INSTRUCTORES: </h1>
  </div>
  <div class="col-md-4">
    <br>
    <a href="<?php echo site_url('instructores/nuevo'); ?>" class="btn btn-primary">
      <i class="glyphicon glyphicon-plus"></i>
      Agregar Instructor
    </a>
  </div>
</div>
<br>
<!-- ifelse y tabulador -->
<?php if ($instructores): ?>
  <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th>ID</th>
        <th>CÉDULA</th>
        <th>PRIMER APELLIDO</th>
        <th>SEGUNDO APELLIDO</th>
        <th>NOMBRES</th>
        <th>TÍTULO</th>
        <th>TELÉFONO</th>
        <th>DIRECCIÓN</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($instructores as $filatemporal): ?>
        <tr>
          <td>
            <?php echo $filatemporal->id_ins?>
          </td>
          <td>
            <?php echo $filatemporal->cedula_ins?>
          </td>
          <td>
            <?php echo $filatemporal->primer_apellido_ins?>
          </td>
          <td>
            <?php echo $filatemporal->segundo_apellido_ins?>
          </td>
          <td>
            <?php echo $filatemporal->nombres_ins?>
          </td>
          <td>
            <?php echo $filatemporal->titulo_ins?>
          </td>
          <td>
            <?php echo $filatemporal->telefono_ins ?>
          </td>
          <td>
            <?php echo $filatemporal->direccion_ins ?>
          </td>
          <td class="text-center">
            <a href="<?php echo site_url(); ?>/Instructores/editar/<?php echo $filatemporal->id_ins?>" title="Editar Instructor" style="color:yellow;"><i class="mdi mdi-grease-pencil"></i></a>

            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="<?php echo site_url(); ?>/Instructores/eliminar/<?php echo $filatemporal->id_ins?>" title="Eliminar Instructor" onclick="return confirm('¿Estás seguro de eliminar de forma permanente el registro seleccionado?');" style="color:red;"><i class="mdi mdi-delete-forever"></i></a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
  <!-- <?php print_r($instructores); ?> -->
<?php else: ?>
<h1>NO HAY DATOS DE INSTRUCTORES</h1>
<?php endif; ?>

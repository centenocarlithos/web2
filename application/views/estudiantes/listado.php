<div class="row">
  <div class="col-md-8">
    <h1>LISTADO DE ESTUDIANTES: </h1>
  </div>
  <div class="col-md-4">
    <br>
    <a href="<?php echo site_url('estudiantes/registro'); ?>" class="btn btn-primary">
      <i class="glyphicon glyphicon-plus"></i>
      Agregar Estudiante
    </a>
  </div>
</div>
<br>
<!-- ifelse y tabulador -->
<?php if ($estudiantes): ?>
  <table class="table table-striped table-bordered">
    <thead>
      <tr>
        <th>ID</th>
        <th>CÉDULA</th>
        <th>APELLIDOS</th>
        <th>NOMBRES</th>
        <th>CURSO</th>
        <th>TELÉFONO</th>
        <th>DIRECCIÓN</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($estudiantes as $filatemporal): ?>
        <tr>
          <td>
            <?php echo $filatemporal->id_es?>
          </td>
          <td>
            <?php echo $filatemporal->cedula_es?>
          </td>
          <td>
            <?php echo $filatemporal->apellido_es?>
          </td>
          <td>
            <?php echo $filatemporal->nombre_es?>
          </td>
          <td>
            <?php echo $filatemporal->curso_es?>
          </td>
          <td>
            <?php echo $filatemporal->telefono_es?>
          </td>
          <td>
            <?php echo $filatemporal->direccion_es?>
          </td>
          <td class="text-center">
            <a href="#" title="Editar Estudiante" style="color:blue;""><i class="glyphicon glyphicon-pencil"></i></a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="<?php echo site_url(); ?>/Estudiantes/eliminar/<?php echo $filatemporal->id_es?>" title="Eliminar Estudiante" onclick="return confirm('¿Estás seguro de eliminar de forma permanente el registro seleccionado?');" style="color:red;"><i class="glyphicon glyphicon-trash"></i></a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
  <!-- <?php print_r($estudiantes); ?> -->
<?php else: ?>
<h1>NO HAY DATOS DE ESTUDIANTES</h1>
<?php endif; ?>

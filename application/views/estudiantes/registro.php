<h1>REGISTRO ESTUDIANTES</h1>
<form class="" action="<?php echo site_url(); ?>/estudiantes/guardar" method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Cédula:</label>
          <br>
          <input type="number"
          placeholder="Ingrese la cédula"
          class="form-control"
          name="cedula_es" value="" id="cedula_es">
      </div>
      <div class="col-md-4">
          <label for="">Apellidos:</label>
          <br>
          <input type="text"
          placeholder="Ingrese sus apellidos"
          class="form-control"S
          name="apellido_es" value="" id="apellido_es">
      </div>
      <div class="col-md-4">
        <label for="">Nombres:</label>
        <br>
        <input type="text"
        placeholder="Ingrese sus nombres"
        class="form-control"
        name="nombre_es" value="" id="nombre_es">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Curso:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el curso"
          class="form-control"
          name="curso_es" value="" id="curso_es">
      </div>
      <div class="col-md-4">
        <label for="">Teléfono:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el telefono"
        class="form-control"
        name="telefono_es" value="" id="telefono_es">
      </div>
      <div class="col-md-4">
          <label for="">Dirección:</label>
          <br>
          <input type="text"
          placeholder="Ingrese la direccion"
          class="form-control"
          name="direccion_es" value="" id="direccion_es">
      </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/estudiantes/listado" class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>

<?php
    class Estudiantes extends CI_Controller
    {

      function __construct()
      {
        parent::__construct();
        //cargar todos los modelos que necesitemos
        $this->load->model('Estudiante');
      }
//Funcion que renderiza la vista index
      public function listado(){
        $data['estudiantes']=$this->Estudiante->obtenerTodo();
        $this->load->view('header');
        $this->load->view('estudiantes/listado',$data);
        $this->load->view('footer');
      }

      public function registro(){
        $this->load->view('header');
        $this->load->view('estudiantes/registro');
        $this->load->view('footer');
      }

      public function guardar(){
        $datosNuevoEstudiante=array(
          "cedula_es"=>$this->input->post('cedula_es'),
          "apellido_es"=>$this->input->post('apellido_es'),
          "nombre_es"=>$this->input->post('nombre_es'),
          "curso_es"=>$this->input->post('curso_es'),
          "telefono_es"=>$this->input->post('telefono_es'),
          "direccion_es"=>$this->input->post('direccion_es')
        );
        //imprime los datos del array que creamos
        //print_r($datosNuevoInstructor);
        if ($this->Estudiante->insertar($datosNuevoEstudiante)) {
          redirect('Estudiantes/listado');
        } else {
          echo "<h1>ERROR INSERTAR DATOS DEL ESTUDIANTE</h1>";
        }
      }

      //funcion eliminar instructores
      public function eliminar($id_es){
        // echo $id_ins;
        if ($this->Estudiante->borrar($id_es)){//invocando al modelo
          redirect('Estudiantes/listado');
        } else {
          echo "ERROR AL BORRAR ESTUDIANTE:(";
        }
      }
    }//Ciere de la clase
  ?>

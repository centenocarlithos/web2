<?php
    class Instructores extends CI_Controller
    {

      function __construct()
      {
        parent::__construct();
        //cargar todos los modelos que necesitemos
        $this->load->model('Instructor');
        if (!$this->session->userdata("conectado")) {
          redirect ("welcome/login");
        }

      }
      //Funcion que renderiza la vista index
      //Listado
      public function index(){
        $data['instructores']=$this->Instructor->obtenerTodos();
        //Imprime los datos
        // print_r($instructores);
        $this->load->view('header');
        $this->load->view(
          'instructores/index',$data);
        $this->load->view('footer');
      }

      public function nuevo(){
        $this->load->view('header');
        $this->load->view('instructores/nuevo');
        $this->load->view('footer');
      }

      public function guardar(){
        $datosNuevoInstructor=array(
          "cedula_ins"=>$this->input->post('cedula_ins'),
          "primer_apellido_ins"=>$this->input->post('primer_apellido_ins'),
          "segundo_apellido_ins"=>$this->input->post('segundo_apellido_ins'),
          "nombres_ins"=>$this->input->post('nombres_ins'),
          "titulo_ins"=>$this->input->post('titulo_ins'),
          "telefono_ins"=>$this->input->post('telefono_ins'),
          "direccion_ins"=>$this->input->post('direccion_ins')
        );
        //imprime los datos del array que creamos
        //print_r($datosNuevoInstructor);
        //$this->Instructor->insertar($datosNuevoInstructor);
        if ($this->Instructor->insertar($datosNuevoInstructor)) {
          $this->session->set_flashdata("confirmacion","Instructor guardado exitosamente");
        }else{
          //embebiendo codigo html dentor de php
          $this->session->set_flashdata("error","Error al guardar intente otra vez :/ ");
        }
        redirect('instructores/index');
      }
      //funcion eliminar instructores
      public function eliminar($id_ins){
        // echo $id_ins;
        if ($this->Instructor->borrar($id_ins)){//invocando al modelo
          $this->session->set_flashdata("confirmacion","Instructor eliminado exitosamente");
        } else {
          $this->session->set_flashdata("error","Error al eliminar intente otra vez :/ ");
        }
        redirect('Instructores/index');
      }

      //funcion que renderiza el formulario editar
      public function editar($id_ins){
        $data["instructorEditar"]=$this->Instructor->obtenerPorId($id_ins);
        $this->load->view('header');
        $this->load->view('instructores/editar',$data);
        $this->load->view('footer');
      }

      //Proceso de actualizacion
      public function procesarActualizacion()
      {
        $datosEditados=array(
          "cedula_ins"=>$this->input->post('cedula_ins'),
          "primer_apellido_ins"=>$this->input->post('primer_apellido_ins'),
          "segundo_apellido_ins"=>$this->input->post('segundo_apellido_ins'),
          "nombres_ins"=>$this->input->post('nombres_ins'),
          "titulo_ins"=>$this->input->post('titulo_ins'),
          "telefono_ins"=>$this->input->post('telefono_ins'),
          "direccion_ins"=>$this->input->post('direccion_ins')
        );
        $id_ins=$this->input->post("id_ins");
        if ($this->Instructor->actualizar($id_ins,$datosEditados)) {
          $this->session->set_flashdata("confirmacion","Instructor actualizado exitosamente");
        } else {
          $this->session->set_flashdata("error","Error al actualizar intente otra vez :/ ");
        }
        redirect("instructores/index");

      }


    }//Ciere de la clase
  ?>

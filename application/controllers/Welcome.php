<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('header');
		$this->load->view('welcome_message');
		$this->load->view('footer');
	}

	public function login()
	{
		$this->load->view("login");
	}

	public function iniciarSesion(){
   	 $this->load->model("Usuario");
   	 $email=$this->input->post("email_usu");
   	 $password=$this->input->post("password_usu");
   	 $usuarioConectado=$this->Usuario->obtenerPorEmailPassword($email,$password);
   	 if($usuarioConectado){
   					 $this->session->set_userdata("conectado",
   					 $usuarioConectado);
   					 redirect("welcome/index");
   	 }else{
   		 redirect("welcome/login");
   	 }
    }


}
